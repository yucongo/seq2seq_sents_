import requests
from pyquery import PyQuery as pq

url = 'https://bj.5i5j.com/zufang/41314775.html'
timeout = 88
try:
    resp = requests.get(url, timeout=tuple([timeout] * 2))
    resp.raise_for_status()
except Exception as exc:
    raise SystemExit(exc)

ssselector = 'body > div.main.container > div.detail-main > div.box-left.fl > div:nth-child(6) > div'

doc = pq(resp.text)
print(doc(ssselector).text())