'''github/gitlab colab version

remove personal lib
sys.path = reduce(lambda x, y: x + [y] if 'my' not in y else x, sys.path, [])

need to save model and update gitlab

win in myapps\gitlab_test
atechk vultr dl\gitlab

webbrowser
    webbrowser.open(url, new=0, autoraise=True)¶
    webbrowser.open_new(url)
    webbrowser.open_new_tab(url)¶
    webbrowser('C:\dl\Dropbox\mat-dir\myapps\gitlab_test\seq2seq_sents\vultr_sent_seq2seq_translation.py')

interactive: subprocess.run subprocess.Popen https://stackoverflow.com/questions/163542/python-how-do-i-pass-a-string-into-subprocess-popen-using-the-stdin-argument

print(p.returncode)  # 0

'''
# pylint: disable=C0103

import sys
import os
from pathlib import Path
from functools import reduce
import subprocess as sp
import shlex

sys.path = reduce(lambda x, y: x + [y] if 'my' not in y else x, sys.path, [])
if not Path(os.getcwd()).stem in ['seq2seq_sents', ]:
    try:
        os.chdir('seq2seq_sents')
    except Exception as exc:
        print(exc)
        raise SystemExit('Should chdir to seq2seq_sents')
# ### make sure it's in seq2seq_sents

# ### git sync pull and push
# cmd = 'git pull && git add . && git commit -m "py " && git push'
# p = sp.Popen(cmd, shell=True)  # OK
# p.communicate()  # blocking
# sp.Popen(cmd, shell=True)  # nonblocking
# sp.Popen('ls -rt')

# universal_newlines = True, str out and in rather than bytes
# sp.run('git pull && git add . && git commit -m "py" && git push', shell=True, universal_newlines=True)  # ok

# sp.run(
#     'git pull && git add . && git commit -m "py" && git push',
#     shell=True, universal_newlines=True,
#     stdout=sp.PIPE, stderr=sp.PIPE,
#     )  # OK, out.stdout, out.stderr, out.returncode

# sp.run('git pull')  # blocking
# sp.run('git add .')
# sp.run('git commit -m "py"')
# sp.run('git push')
# sp.run('ls -lrt')

# cmd = shlex.split('git add .')
# sp.run(cmd)  # blocking?

# cmd = shlex.split('git commit -m "py "')
# sp.run(cmd)
# cmd = shlex.split('git push')
# sp.run(cmd)

# ### run p
# %load filename.py
# %load -r line1-line2 line1:line2 line1..line2

# remove personal lib
sys.path = reduce(lambda x, y: x + [y] if 'my' not in y else x, sys.path, [])

# run in pytorch activate-conda && activate theano
# refer to vultr_sent_seq2seq_translation.py


# from __future__ import division, print_function, unicode_literals

import sys
from pathlib import Path
import math
import pandas as pd
import random
import re
# import string
import time
import unicodedata
from io import open
from functools import reduce

# import matplotlib.pyplot as plt
# import matplotlib.ticker as ticker
# import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim

# from gen_red_red_n import gen_red_red_n

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
random.seed(-1)


SOS_token = 0
EOS_token = 1


class Lang:
    def __init__(self, name):
        self.name = name
        self.word2index = {}
        self.word2count = {}
        self.index2word = {0: "SOS", 1: "EOS", 2: 'unk'}
        self.n_words = 3  # Count SOS and EOS and unk

    def addSentence(self, sentence):
        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1


######################################################################
# The files are all in Unicode, to simplify we will turn Unicode
# characters to ASCII, make everything lowercase, and trim most
# punctuation.
#

# Turn a Unicode string to plain ASCII, thanks to
# http://stackoverflow.com/a/518232/2809427
def unicodeToAscii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
    )

# Lowercase, trim, and remove non-letter characters


def normalizeString(s):
    s = unicodeToAscii(s.lower().strip())

    # remove commas in numbers
    s = re.sub(r',(\d{3})', r'\1', s)
    s = re.sub(r"([.!?,])", r" \1", s)
    s = re.sub(r"[^a-zA-Z.!?\d,]+", r" ", s)
    return s


######################################################################
# To read the data file we will split the file into lines, and then split
# lines into pairs. The files are all English → Other Language, so if we
# want to translate from Other Language → English I added the ``reverse``
# flag to reverse the pairs.
#

def readLangs(lang1, lang2, reverse=False, sent=False):
    print("Reading lines...")

    # Read the file and split into lines
    if sent:
        lines = open('data/%s-%s_sents.txt' % (lang1, lang2), encoding='utf-8').\
            read().strip().split('\n')
    else:
        lines = open('data/%s-%s.txt' % (lang1, lang2), encoding='utf-8').\
            read().strip().split('\n')

    # Split every line into pairs and normalize
    pairs = [[normalizeString(s) for s in l.split('\t')] for l in lines]

    # Reverse pairs, make Lang instances
    if reverse:
        pairs = [list(reversed(p)) for p in pairs]
        input_lang = Lang(lang2)
        output_lang = Lang(lang1)
    else:
        input_lang = Lang(lang1)
        output_lang = Lang(lang2)

    return input_lang, output_lang, pairs


######################################################################
# Since there are a *lot* of example sentences and we want to train
# something quickly, we'll trim the data set to only relatively short and
# simple sentences. Here the maximum length is 10 words (that includes
# ending punctuation) and we're filtering to sentences that translate to
# the form "I am" or "He is" etc. (accounting for apostrophes replaced
# earlier).
#

# ''' #

# sent max: 95 (red sorghom ch1)
MAX_LENGTH = 20  # 100
MAX_LENGTH = 60  # 100
hidden_size = 128  # #modi
hidden_size = 300  # #modi
hidden_size = 600  # #modi
# iter_n2000, 30min, loss5.2->3.6-30min->3.2

dir_name = 'data'
filename = 'red-red_sents.txt'
df_sent = pd.read_csv(
    Path(dir_name, filename),
    sep='\t',
    names=['left', 'right'],
)


def filterPair(p):
    return len(p[0].split(' ')) < MAX_LENGTH and \
        len(p[1].split(' ')) < MAX_LENGTH
        # and p[1].startswith(eng_prefixes)


def filterPairs(pairs):
    # return [pair for pair in pairs if filterPair(pair)]
    # trimmed to MAX_LENGTH
    return [[' '.join(pair[0].split()[:MAX_LENGTH - 1])]*2 for pair in pairs]


def prepareData(lang1, lang2, reverse=False, sent=False):
    # input_lang, output_lang, pairs0 = readLangs(lang1, lang2, reverse)
    input_lang, output_lang, pairs0 = readLangs(lang1, lang2, reverse, sent)
    print("Read %s sentence pairs" % len(pairs0))
    pairs1 = filterPairs(pairs0)
    print("Trimmed to %s sentence pairs" % len(pairs1))
    print("Counting words...")
    for pair in pairs1:
        input_lang.addSentence(pair[0])
        output_lang.addSentence(pair[1])
    print("Counted words:")
    print(input_lang.name, input_lang.n_words)
    print(output_lang.name, output_lang.n_words)
    return input_lang, output_lang, pairs1


# input_lang, output_lang, pairs = prepareData('eng', 'fra', True)
# input_lang, output_lang, pairs = prepareData('eng', 'eng', True)

src_lang = 'red' + str(MAX_LENGTH)  # modi
src_lang = 'red'  # modi
# input_lang, output_lang, pairs = prepareData('red', 'red', True)
input_lang, output_lang, pairs = prepareData(src_lang, src_lang, True, True)

print(random.choice(pairs))


class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)


class DecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size):
        super(DecoderRNN, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        output = self.embedding(input).view(1, 1, -1)
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)


class AttnDecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, dropout_p=0.1, max_length=MAX_LENGTH):
        super(AttnDecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.dropout_p = dropout_p
        self.max_length = max_length

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.dropout = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(self.hidden_size, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, input, hidden, encoder_outputs):
        embedded = self.embedding(input).view(1, 1, -1)
        embedded = self.dropout(embedded)

        attn_weights = F.softmax(
            self.attn(torch.cat((embedded[0], hidden[0]), 1)), dim=1)
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                 encoder_outputs.unsqueeze(0))

        output = torch.cat((embedded[0], attn_applied[0]), 1)
        output = self.attn_combine(output).unsqueeze(0)

        output = F.relu(output)
        output, hidden = self.gru(output, hidden)

        output = F.log_softmax(self.out(output[0]), dim=1)
        return output, hidden, attn_weights

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)


def indexesFromSentence(lang, sentence):
    # return [lang.word2index[word] for word in sentence.split(' ')]
    return [lang.word2index.get(word, 2) for word in sentence.split(' ')]
    # set to 2 (unk) if not in lang.word2index


def tensorFromSentence(lang, sentence):
    indexes = indexesFromSentence(lang, sentence)
    indexes.append(EOS_token)
    return torch.tensor(indexes, dtype=torch.long, device=device).view(-1, 1)


def tensorsFromPair(pair):
    input_tensor = tensorFromSentence(input_lang, pair[0])
    target_tensor = tensorFromSentence(output_lang, pair[1])
    return (input_tensor, target_tensor)


teacher_forcing_ratio = 0.5


def train(input_tensor, target_tensor, encoder, decoder, encoder_optimizer, decoder_optimizer, criterion, max_length=MAX_LENGTH):
    encoder_hidden = encoder.initHidden()

    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    input_length = input_tensor.size(0)
    target_length = target_tensor.size(0)

    encoder_outputs = torch.zeros(max_length, encoder.hidden_size, device=device)

    loss = 0

    for ei in range(input_length):
        encoder_output, encoder_hidden = encoder(
            input_tensor[ei], encoder_hidden)
        encoder_outputs[ei] = encoder_output[0, 0]

    decoder_input = torch.tensor([[SOS_token]], device=device)

    decoder_hidden = encoder_hidden

    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False

    if use_teacher_forcing:
        # Teacher forcing: Feed the target as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            loss += criterion(decoder_output, target_tensor[di])
            decoder_input = target_tensor[di]  # Teacher forcing

    else:
        # Without teacher forcing: use its own predictions as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            topv, topi = decoder_output.topk(1)
            decoder_input = topi.squeeze().detach()  # detach from history as input

            loss += criterion(decoder_output, target_tensor[di])
            if decoder_input.item() == EOS_token:
                break

    loss.backward()

    encoder_optimizer.step()
    decoder_optimizer.step()

    return loss.item() / target_length


def asMinutes(s):
    m = math.floor(s / 60)
    s -= m * 60  # m, s = divmod(s, 60)
    return '%dm %ds' % (m, s)


def timeSince(since, percent):
    now = time.time()
    s = now - since
    es = s / (percent)
    rs = es - s
    return '%s (- %s)' % (asMinutes(s), asMinutes(rs))


def trainIters(encoder, decoder, n_iters, print_every=1000, plot_every=100, learning_rate=0.01):
    start = time.time()
    plot_losses = []
    print_loss_total = 0  # Reset every print_every
    plot_loss_total = 0  # Reset every plot_every

    encoder_optimizer = optim.SGD(encoder.parameters(), lr=learning_rate)
    decoder_optimizer = optim.SGD(decoder.parameters(), lr=learning_rate)
    training_pairs = [tensorsFromPair(random.choice(pairs))
                      for i in range(n_iters)]
    criterion = nn.NLLLoss()

    for iter in range(1, n_iters + 1):
        training_pair = training_pairs[iter - 1]
        input_tensor = training_pair[0]
        target_tensor = training_pair[1]

        loss = train(input_tensor, target_tensor, encoder,
                     decoder, encoder_optimizer, decoder_optimizer, criterion)
        print_loss_total += loss
        plot_loss_total += loss

        if iter % print_every == 0:
            print_loss_avg = print_loss_total / print_every
            print_loss_total = 0
            print('%s (%d %d%%) %.4f' % (timeSince(start, iter / n_iters),
                                         iter, iter / n_iters * 100, print_loss_avg))

            # save and git sync
            torch.save(encoder1, 'colab_encoder1.pkl')
            torch.save(attn_decoder1, 'colab_attn_decoder1.pkl')
            out = sp.run('git add . && git commit -m "py" && git push', shell=True, universal_newlines=True)
            # if out.stdout:
            #     print(out.stdout)
            # if out.stderr:
            #     print(out.stderr)

        if iter % plot_every == 0:
            plot_loss_avg = plot_loss_total / plot_every
            plot_losses.append(plot_loss_avg)
            plot_loss_total = 0

    if sys.platform in ['win32', ]:
        showPlot(plot_losses)


import matplotlib.pyplot as plt
if sys.platform in ['win32', ]:
    plt.switch_backend('Qt5Agg')


def showPlot(points):
    plt.figure()
    fig, ax = plt.subplots()
    # this locator puts ticks at regular intervals
    loc = ticker.MultipleLocator(base=0.2)
    ax.yaxis.set_major_locator(loc)
    plt.plot(points)


def evaluate(encoder, decoder, sentence, max_length=MAX_LENGTH):
    if len(sentence.split()) > MAX_LENGTH - 1:
        sentence = ' '.join(sentence.split()[:MAX_LENGTH - 1])
    with torch.no_grad():
        input_tensor = tensorFromSentence(input_lang, sentence)
        input_length = input_tensor.size()[0]
        encoder_hidden = encoder.initHidden()

        encoder_outputs = torch.zeros(
            max_length, encoder.hidden_size, device=device)

        for ei in range(input_length):
            encoder_output, encoder_hidden = encoder(input_tensor[ei],
                                                     encoder_hidden)
            encoder_outputs[ei] += encoder_output[0, 0]

        decoder_input = torch.tensor([[SOS_token]], device=device)  # SOS

        decoder_hidden = encoder_hidden

        decoded_words = []
        decoder_attentions = torch.zeros(max_length, max_length)

        for di in range(max_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            decoder_attentions[di] = decoder_attention.data
            topv, topi = decoder_output.data.topk(1)
            if topi.item() == EOS_token:
                decoded_words.append('<EOS>')
                break
            else:
                decoded_words.append(output_lang.index2word[topi.item()])

            decoder_input = topi.squeeze().detach()

        return decoded_words, decoder_attentions[:di + 1]


######################################################################
# We can evaluate random sentences from the training set and print out the
# input, target, and output to make some subjective quality judgements:
#

def evaluateRandomly(encoder, decoder, n=10):
    for i in range(n):
        pair = random.choice(pairs)
        print('>', pair[0])
        print('=', pair[1])
        output_words, attentions = evaluate(encoder, decoder, pair[0])
        output_sentence = ' '.join(output_words)
        print('<', output_sentence)
        print('')

# ###-


print('Training....')
# hidden_size = 256  # modi
if Path('colab.pkl').exists():
    torch.load('colab_encoder1.pkl')
else:
    encoder1 = EncoderRNN(input_lang.n_words, hidden_size).to(device)

if Path('colba_attn_decoder1.pkl').exists():
    torch.load('colab_attn_decoder1.pkl')
else:
    attn_decoder1 = AttnDecoderRNN(hidden_size, output_lang.n_words, dropout_p=0.1).to(device)

# trainIters(encoder1, attn_decoder1, 75000, print_every=5000)
trainIters(encoder1, attn_decoder1, 2000, print_every=100)
print('Training ends')
evaluateRandomly(encoder1, attn_decoder1)
# ''' #

iter_n = 4000
iter_n = 1000
iter_n = 2000
iter_n = 3000
learning_rate = 0.001
learning_rate = 0.01

# trainIters(encoder1, attn_decoder1, iter_n, print_every=100, learning_rate=0.01)
trainIters(encoder1, attn_decoder1, iter_n,
           print_every=100, learning_rate=learning_rate)
print('Training ends')

evaluateRandomly(encoder1, attn_decoder1)

# ###-
# git config --global alias.gitpp '!git pull && git add -A && git commit && git push'
torch.save(encoder1, 'colab_encoder1.pkl')
torch.save(attn_decoder1, 'colab_attn_decoder1.pkl')
out = sp.run('git pull && git add . && git commit -m "py" && git push', shell=True, universal_newlines=True)
if out.stdout:
    print(out.stdout)
if out.stderr:
    print(out.stderr)
