'''process text to generate sents
tokenize first;
normalize_string, cleanse except [a-zA-Z] and [.,?!\d]

trim front if sents length longer than len_

'''
# pylint: disable=C0103

import os
import sys
from pathlib import Path
import importlib
import pandas as pd

# from segtok.segmenter import split_single

from switch_to import switch_to
# import normalize_string
# importlib.reload(normalize_string)
from normalize_string import normalize_string as normalize_string
from nltk.tokenize import sent_tokenize  # , word_tokenize

# import sys; len_ = sys.maxsize
# len_ = 20


def gen_red_red_n_sents(len_=sys.maxsize, slice=None):
    ''' gen redn-redn.txt '''

    dir_name = 'data'
    if Path(dir_name).exists():
        with switch_to(dir_name):
            print(os.getcwd())
    else:
        print(os.getcwd())
        raise SystemExit(f'The dir [{dir_name}] does not exist.')

    encsv_file = 'red_ch1_en.csv'
    zhcsv_file = 'red_ch1_zh.csv'

    df_en = pd.read_csv(Path(dir_name, encsv_file), names=['idx', 'en', 'enmt'])
    df_zh = pd.read_csv(Path(dir_name, zhcsv_file), names=['idx', 'zh', 'zhenmt'])

    # src_row1, src_row2 = 1, 16 + 1
    # tgt_row1, tgt_row2 = 1, 11 + 1
    # slice = ((src_row1, src_row2), (tgt_row1, tgt_row2))
    if slice is not None:
        df_en = df_en.iloc[slice[0][0] - 1: slice[0][1]]
        df_zh = df_zh.iloc[slice[1][0] - 1: slice[1][1]]

    # ' '.join(normalizeString(df_en.en[1]).split()[: len_])

    temp = []
    for field in ['en', 'enmt']:
        for elm in df_en[field]:
            for sent in sent_tokenize(elm):
                temp += [' '.join(normalize_string(sent).split()[:len_ - 1])]

    for field in ['zhenmt']:
        for elm in df_zh[field]:
            for sent in sent_tokenize(elm):
                temp += [' '.join(normalize_string(sent).split()[:len_ - 1])]

    # temp_len = list(map(len, map(str.split, temp)))
    # rmap = lambda x, y: map(y, x)
    # list(reduce(lambda x, y: map(y, x), [str.split, len], temp))
    # max(temp_len): 95
    # trim to 10:
    # list(reduce(lambda x, y: map(y, x), [str.split, lambda x: x[:10], ], temp))

    df = pd.DataFrame({'left': temp, 'right': temp})

    slice_name = ''
    if slice is not None:
        slice_name = '_test'
    if len_ == sys.maxsize:
        out_filename = f'red-red_sents{slice_name}.txt'
    else:
        out_filename = f'red{len_}-red{len_}_sents{slice_name}.txt'

    df.to_csv(Path(dir_name, out_filename), header=False, index=False,sep='\t', )
    return df

if __name__ == '__main__':
    slice = ((1, 17), (1, 12))
    gen_red_red_n_sents(slice=slice)
