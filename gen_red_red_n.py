'''
gen_red_red_n.txt
'''
# pylint: disable=C0103

import os
from pathlib import Path
import importlib
import pandas as pd
from switch_to import switch_to
import normalize_string
# importlib.reload(normalize_string)
from normalize_string import normalize_string as normalizeString

# len_ = 20


def gen_red_red_n(len_):
    ''' gen redn-redn.txt '''
    dir_name = 'data'
    if Path(dir_name).exists():
        with switch_to(dir_name):
            print(os.getcwd())
    else:
        print(os.getcwd())
        raise SystemExit(f'The dir [{dir_name}] does not exist.')

    encsv_file = 'red_ch1_en.csv'
    zhcsv_file = 'red_ch1_zh.csv'

    df_en = pd.read_csv(Path(dir_name, encsv_file), names=['idx', 'en', 'enmt'])
    df_zh = pd.read_csv(Path(dir_name, zhcsv_file), names=['idx', 'zh', 'zhenmt'])

    # ' '.join(normalizeString(df_en.en[1]).split()[: len_])

    temp = []
    for field in ['en', 'enmt']:
        for elm in df_en[field]:
            temp += [' '.join(normalizeString(elm).split()[:len_ - 1])]
    for field in ['zhenmt']:
        for elm in df_zh[field]:
            temp += [' '.join(normalizeString(elm).split()[:len_ - 1])]

    df = pd.DataFrame({'left': temp, 'right': temp})

    out_filename = f'red{len_}-red{len_}.txt'
    df.to_csv(Path(dir_name, out_filename), header=False, index=False,sep='\t', )
    return df
