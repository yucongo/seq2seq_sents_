r'''
process data / eng-fra.txt

pip install pylint astroid --pre
 PurPath has no 'exists' member.... fix, does not seem to work
'''
# pylint: disable=C0103, E1101
# # ignored-classes=PurePath
import os
from pathlib import Path
import pandas as pd

file = 'eng-fra.txt'
dir_loc = 'data'
filepath = Path(dir_loc) / Path(file)
msg = 'the File [{}] does not exist.'.format(filepath)
assert filepath.exists(), msg

df = pd.read_csv(
    filepath,
    sep='\t',
    encoding='utf8',
    names=['en', 'fr'],
)
df.fr = df.en  # overwrite fr with en
df.drop_duplicates(inplace=True)
df.to_csv(Path(dir_loc) / 'eng-eng.txt', sep='\t', header=False, index=False)
