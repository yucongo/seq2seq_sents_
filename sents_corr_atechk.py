''' modified from gen_setns in asusfmt'''
# pylint: disable=C0103

import os
import sys
from pathlib import Path
import importlib
import pandas as pd
import numpy as np

# from segtok.segmenter import split_single

from switch_to import switch_to
# import normalize_string
# importlib.reload(normalize_string)
from normalize_string import normalize_string
from nltk.tokenize import sent_tokenize  # , word_tokenize

dir_name = 'data'
if Path(dir_name).exists():
    with switch_to(dir_name):
        print(os.getcwd())
else:
    print(os.getcwd())
    raise SystemExit(f'The dir [{dir_name}] does not exist.')

encsv_file = 'red_ch1_en.csv'
zhcsv_file = 'red_ch1_zh.csv'

df_en = pd.read_csv(Path(dir_name, encsv_file), names=['idx', 'en', 'enmt'])
df_zh = pd.read_csv(Path(dir_name, zhcsv_file), names=['idx', 'zh', 'zhenmt'])

slice = ((1, 17), (1, 12))

if 'slice' in vars():
    df_en = df_en.iloc[slice[0][0] - 1: slice[0][1]]
    df_zh = df_zh.iloc[slice[1][0] - 1: slice[1][1]]


# ' '.join(normalizeString(df_en.en[1]).split()[: len_])

len_ = MAX_LENGTH

for source in ['en', 'enmt', ]:
    temp = []
    for field in [source]:
        for elm in df_en[field]:
            for sent in sent_tokenize(elm):
                temp += [' '.join(normalize_string(sent).split()[:len_ - 1])]
    vars()['sents_' + source] = [elm.strip() for elm in temp if elm.strip()]


for source in ['zhenmt']:
    temp = []
    for field in [source]:
        for elm in df_zh[field]:
            for sent in sent_tokenize(elm):
                temp += [' '.join(normalize_string(sent).split()[:len_ - 1])]
    vars()['sents_' + source] = [elm.strip() for elm in temp if elm.strip()]

torch.dist(encoder_output(encoder1, sents_en[8])[1], encoder_output(encoder1, sents_enmt[9])[1])

dist_en_zhenmt = lambda i, j: torch.dist(encoder_output(encoder1, sents_en[i])[1], encoder_output(encoder1, sents_zhenmt[j])[1])

dist_enmt_zhenmt = lambda i, j: torch.dist(encoder_output(encoder1, sents_enmt[i])[1], encoder_output(encoder1, sents_zhenmt[j])[1]).item()

cos = torch.nn.CosineSimilarity(dim=2, eps=1e-6)

cos_en_enmt = lambda i, j: cos(encoder_output(encoder1, sents_en[i])[1], encoder_output(encoder1, sents_enmt[j])[1]).item()

cos_enmt_zhenmt = lambda i, j: cos(encoder_output(encoder1, sents_enmt[i])[1], encoder_output(encoder1, sents_zhenmt[j])[1]).item()

# i = 9
# cos_vec = [cos_enmt_zhenmt(i, j) for j in range(len(sents_zhenmt))]

delta = 5 + 1
ratio = len(sents_zhenmt) / len(sents_enmt)
for idx in range(len(sents_enmt)):
    low = max(0, int(ratio * idx) - delta)
    high = min(len(sents_zhenmt), int(ratio * idx) + delta)
    cos_vec = [cos_enmt_zhenmt(idx, j) for j in range(len(sents_zhenmt))]
    argmax = np.argmax(cos_vec[low: high])
    if cos_vec[argmax + low] > 0.78:
        print(idx, argmax + low, cos_vec[argmax + low],'\n', sents_enmt[idx], '\n', sents_zhenmt[argmax + low] )
