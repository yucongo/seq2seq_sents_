import os
from pathlib import Path
import pandas as pd
from switch_to import switch_to
from normalize_string import normalize_string as normalizeString

encsv_file = 'red_ch1_en.csv'
zhcsv_file = 'red_ch1_zh.csv'
dir_name = 'data'

df_en = pd.read_csv(Path(dir_name, encsv_file), names=['idx', 'en', 'enmt'])
df_zh = pd.read_csv(Path(dir_name, zhcsv_file), names=['idx', 'zh', 'zhenmt'])

temp = []
for field in ['en', 'enmt']:
    for elm in df_en[field]:
        temp += [' '.join(normalizeString(elm).split())]
for field in ['zhenmt']:
    for elm in df_zh[field]:
        temp += [' '.join(normalizeString(elm).split())]

len_vec = [len(elm.split()) for elm in temp]

sum(len_vec) / len(len_vec)  # 88.88
# s0 = temp[-1]
# for elm in '.!?,':
#     # print(elm)
#     s0 = s0.replace(elm, '').strip()
#     s1 = re.sub(r'[.!?,]', '', temp[-1]).strip()

char_vec = [len(''.join(re.sub(r'[.!?,]', '', elm).strip().split()))
            for elm in temp]
sum(char_vec) / len(char_vec)   # 338.7 chars/para
word_len = sum(char_vec)/sum(len_vec)  # 3.810625 chars/word
